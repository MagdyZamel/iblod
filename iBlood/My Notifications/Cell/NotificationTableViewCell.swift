//
//  RequestTableViewCell.swift
//  iBlood
//
//  Created by Aya Amr on 4/23/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit

protocol NotificationTableViewDelegate: class {
    func declineButtonTapped(view: NotificationTableViewCell)
    func acceptButtonTapped(view: NotificationTableViewCell)
}

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var blood: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    
    var delegate: NotificationTableViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func acceptButtonTapped(_ sender: Any) {
        delegate?.acceptButtonTapped(view: self)
    }
    
    @IBAction func declineButtonTApped(_ sender: Any) {
        delegate?.declineButtonTapped(view: self)
    }
}
