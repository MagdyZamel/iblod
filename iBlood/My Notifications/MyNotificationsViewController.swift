//
//  MyNotificationsViewController.swift
//  iBlood
//
//  Created by Aya Amr on 4/23/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit
import Alamofire

class MyNotificationsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var cellReusableID = "NotificationTableViewCell"
    private var searchController:UISearchController!
    var requests = [Notification]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func getNotifications() {
        let headers = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json",
            "cache-control": "no-cache",
            "Postman-Token": "d171615d-f18a-4714-aef6-ae3c57522bbf"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(basedomain)notifications")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                var json: NSDictionary = [:]
                guard let data = data else {
                    return
                }
                
                do {
                    var requests = [Notification]()
                    let ssss =  try! JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                    ssss.forEach({ (request) in
                        if let x = Notification(JSON: request) {
                            if x.status == nil {
                                requests.append(x)
                            }
                        }
                    })
                    DispatchQueue.main.async {
                        self.requests = requests
                        self.tableView.reloadData()
                    }
                } catch {
                    // Do nothing
                }
            }
        })
        
        dataTask.resume()
    }
    func setupViews(){
        self.navigationController?.navigationItem.largeTitleDisplayMode = .automatic
        setupSearchBar()
        setupNavigationButtons()
        configureTableView()
        getNotifications()
        
        
    }
    
    func setupSearchBar() {
        self.navigationItem.title = "Notifications"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController?.dimsBackgroundDuringPresentation = false
    }
    
    func setupNavigationButtons() {
//        let emergencyButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
//        emergencyButton.setTitleColor(#colorLiteral(red: 1, green: 0.2812636793, blue: 0.3673964143, alpha: 1), for: [])
//        emergencyButton.setTitle("Emergency", for: [])
//        emergencyButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
//        emergencyButton.addTarget(self, action: #selector(self.emergencyButtonTapped), for: UIControl.Event.touchUpInside)
//        let leftBarButton = UIBarButtonItem(customView: emergencyButton)
//        self.navigationItem.leftBarButtonItem = leftBarButton
//
        let addRequestButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
        addRequestButton.setImage(#imageLiteral(resourceName: "icons8-available-updates-25"), for: [])
        addRequestButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
        addRequestButton.addTarget(self, action: #selector(self.addButtonTapped), for: UIControl.Event.touchUpInside)
        let rightBarButton = UIBarButtonItem(customView: addRequestButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func emergencyButtonTapped() {
        let emergencyVC = EmergencyViewController()
        self.navigationController?.pushViewController(emergencyVC, animated: true)
    }
    
    @objc func addButtonTapped() {
        getNotifications()
    }
    
    func configureTableView() {
        let postsCell = UINib(nibName: cellReusableID, bundle: nil)
        tableView.register(postsCell, forCellReuseIdentifier: cellReusableID)
    }
    
}

extension MyNotificationsViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return requests.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReusableID, for: indexPath) as! NotificationTableViewCell
        cell.delegate = self
        cell.name.text = requests[indexPath.section].name ?? "Mohamed Amr"
        cell.blood.text = requests[indexPath.section].blood_type ?? "Mohamed Amr"
        cell.address.text = requests[indexPath.section].address ?? "Nasr City"
        let date = requests[indexPath.section].created_at ?? "Cairo.rehab.mall2"
        
        if date.count > 0 {
            let d = date.split(separator: " ")
            cell.date.text = String(d.first ?? "")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.white
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let feedback = UITableViewRowAction(style: .default, title: "Feedback") { action, index in
            print("hello")
        }
        feedback.backgroundColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        return [feedback]
    }
}

extension MyNotificationsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let acceptedUserVC = AcceptedUserVC()
        let nav = UINavigationController(rootViewController: acceptedUserVC)
        self.present(nav, animated: true, completion: nil)
    }
}

extension MyNotificationsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            //            presenter.updateSearchResults(text: searchText)
        }
    }
}

extension MyNotificationsViewController: NotificationTableViewDelegate {
    func declineButtonTapped(view: NotificationTableViewCell) {
        guard let indexPath = tableView.indexPath(for: view) else { return }
        requests.remove(at: indexPath.section)
        tableView.deleteSections(IndexSet([indexPath.section]), with: .automatic)

        
    }
    //
    func acceptButtonTapped(view: NotificationTableViewCell) {
        guard let indexPath = tableView.indexPath(for: view) else { return }
        view.acceptButton.startAnimating()
        let selectedRequest = requests[indexPath.section].id ?? 0
        
        let headers = [ "Accept": "application/json",
                        "Authorization": "Bearer \(accessToken)"
            
        ]
        let parameters = [
            
                "notification_id": "\(selectedRequest)",
                "status": "1"
            ]
        
        let URL = try! URLRequest(url: "\(basedomain)respond-notification", method: .post, headers: headers)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
        },
            with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let request, _, _):
                    print("success")
                    request.uploadProgress(closure: { (progress_) in
                        print("Upload Progress: \(progress_.fractionCompleted)")
                        print("Upload totalUnitCount: \(progress_.totalUnitCount)")
                    })
                    request.validate(statusCode: 200..<500)
                    request.responseJSON { response in
                        
                        debugPrint("uploadRegistration: \(response)")
                        if response.result.isSuccess {
                            DispatchQueue.main.async {
                                view.acceptButton.stopAnimating()
                                self.requests.remove(at: indexPath.section)
                                self.tableView.deleteSections(IndexSet([indexPath.section]), with: .fade)
                                self.showSuccessAlert(withTitle: "Success", message: "Request has been accepted");
                                
                            }
                            
                        } else { //FAILURE
                            DispatchQueue.main.async {
                                view.acceptButton.stopAnimating()
                                self.showErrorAlert(withTitle: "Error", message: "SomeThing went wrong");
                            }
                            
                        }
                        
                    }
                case .failure(let errorType):
                    DispatchQueue.main.async {
                        view.acceptButton.stopAnimating()
                        self.showErrorAlert(withTitle: " Error", message: "Connection Lost");
                        
                    }
                }
        })
        

        
    }
    
    
}
