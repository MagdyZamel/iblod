//
//  MyRequestsViewController.swift
//  iBlood
//
//  Created by Aya Amr on 4/23/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit

class MyRequestsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var cellReusableID = "RequestTableViewCell"
    private var searchController:UISearchController!
    var requests = [SingleRequest]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews(){
        self.navigationController?.navigationItem.largeTitleDisplayMode = .automatic
        setupSearchBar()
        setupNavigationButtons()
        configureTableView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getRequests()
    }
    
    func getRequests() {
        
        let headers = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json",
            "cache-control": "no-cache",
            "Postman-Token": "d171615d-f18a-4714-aef6-ae3c57522bbf"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(basedomain)my-requests")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                var json: NSDictionary = [:]
                guard let data = data else {
                    return
                }
                
                do {
                    var requests = [SingleRequest]()
                    let ssss =  try! JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                    ssss.forEach({ (request) in
                        if let x = SingleRequest(JSON: request) {
                            requests.append(x)
                        }
                    })
                    DispatchQueue.main.async {
                        self.requests = requests
                        self.tableView.reloadData()
                    }
                } catch {
                    // Do nothing
                }
            }
        })
        
        dataTask.resume()
    }
    
    func setupSearchBar() {
        self.navigationItem.title = "My Requests"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController?.dimsBackgroundDuringPresentation = false
    }
    
    func setupNavigationButtons() {
        let emergencyButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
        emergencyButton.setTitleColor(#colorLiteral(red: 1, green: 0.2812636793, blue: 0.3673964143, alpha: 1), for: [])
        emergencyButton.setTitle("Emergency", for: [])
        emergencyButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
        emergencyButton.addTarget(self, action: #selector(self.emergencyButtonTapped), for: UIControl.Event.touchUpInside)
        let leftBarButton = UIBarButtonItem(customView: emergencyButton)
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        let addRequestButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
        addRequestButton.setImage(#imageLiteral(resourceName: "plus"), for: [])
        addRequestButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
        addRequestButton.addTarget(self, action: #selector(self.addButtonTapped), for: UIControl.Event.touchUpInside)
        let rightBarButton = UIBarButtonItem(customView: addRequestButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func emergencyButtonTapped() {
        let emergencyVC = EmergencyViewController()
        self.navigationController?.pushViewController(emergencyVC, animated: true)
    }
    
    @objc func addButtonTapped() {
        let newRequestVC = NewRequestViewController()
        self.navigationController?.pushViewController(newRequestVC, animated: true)
    }
    
    func configureTableView() {
        let postsCell = UINib(nibName: cellReusableID, bundle: nil)
        tableView.register(postsCell, forCellReuseIdentifier: cellReusableID)
    }
    
}

extension MyRequestsViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return requests.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RequestTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReusableID, for: indexPath) as! RequestTableViewCell
        let date = requests[indexPath.section].created_at ?? "Cairo.rehab.mall2"
        cell.address.text = requests[indexPath.section].address ?? "Cairo.rehab.mall2"
        if date.count > 0 {
            let d = date.split(separator: " ")
            cell.date.text = String(d.first ?? "")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.white
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let feedback = UITableViewRowAction(style: .default, title: "Feedback") { action, index in
            let addRate = AddRateViewController()
            addRate.modalPresentationStyle = .overFullScreen
            self.modalTransitionStyle = .crossDissolve
            self.present(addRate, animated: true)
        }
        feedback.backgroundColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        return [feedback]
    }
}

extension MyRequestsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let acceptedUserVC = AcceptedUserVC()
        acceptedUserVC.requestID = requests[indexPath.section].id ?? 0
        let nav = UINavigationController(rootViewController: acceptedUserVC)
        self.present(nav, animated: true, completion: nil)
    }
}

extension MyRequestsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            //            presenter.updateSearchResults(text: searchText)
        }
    }
}

