//
//  EditCustomLoactionVC.swift
//  HeuristicsOffers
//
//  Created by Magdy Zamel on 9/8/17.
//  Copyright © 2017 Fxlab. All rights reserved.
//

import UIKit
import MapKit

protocol EditCustomLoactionDelegate:class {
    func editCustomLoaction(didPicked location: CLLocationCoordinate2D,controller: EditCustomLoactionVC)
    func editCustomLoaction(didCanceled controller: EditCustomLoactionVC)

}
class EditCustomLoactionVC: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var centerButton: UIButton!
    weak var delegate:EditCustomLoactionDelegate?
    var coordinate:CLLocationCoordinate2D!

    override func viewDidLoad() {
        super.viewDidLoad()
        let span = MKCoordinateSpanMake(0.7, 0.7)
        let region = MKCoordinateRegionMake(coordinate, span)
        self.mapView.setRegion(region, animated: true)

    }

    @IBAction func doneButtonTapped() {
        var touchLocation = centerButton.frame.origin
        touchLocation.y = touchLocation.y + centerButton.frame.height
        touchLocation.x = centerButton.frame.midX

        let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
        delegate?.editCustomLoaction(didPicked: locationCoordinate, controller: self)


    }
    @IBAction func cancelButtonTapped() {
        delegate?.editCustomLoaction(didCanceled: self)

    }

}
