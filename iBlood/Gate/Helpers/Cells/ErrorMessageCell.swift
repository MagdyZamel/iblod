//
//  ErrorMessageCell.swift
//  Edfa3ly
//
//  Created by Mostafa El_sayed on 10/11/17.
//  Copyright © 2017 Robusta. All rights reserved.
//

import UIKit

protocol ErrorMessageView {
    func display(message: String)
}

class ErrorMessageCell: UITableViewCell, ErrorMessageView {

    @IBOutlet weak var titleLabel: UILabel!
   
    func display(message: String) {
        self.titleLabel.text! = message
    }
    
}
