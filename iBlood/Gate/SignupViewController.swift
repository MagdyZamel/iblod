//
//  SignupViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/8/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import Alamofire

let basedomain = "http://138.68.102.152/app/public/index.php/api/"
var accessToken = ""

class SignupViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1

    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count

    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return  pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTextField.text =  pickerData[row]
        UIView.animate(withDuration: 0.3) {
            self.picker.alpha = 0

        }
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var bloodTypeTextField: UITextField!
    @IBOutlet weak var nationalIdTextField: UITextField!
    @IBOutlet weak var birthDateTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!

    
    @IBOutlet weak var picker: UIPickerView!
    var pickerData: [String] = ["Male", "Female"]

    let datePicker = UIDatePicker()

    override func viewDidLoad() {
        super.viewDidLoad()
        showDatePicker()
        self.picker.delegate = self
        self.picker.dataSource = self
    }
    
    

    func showDatePicker() {
        //Formate Date
        datePicker.datePickerMode = .date

        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        birthDateTextField.inputAccessoryView = toolbar
        birthDateTextField.inputView = datePicker
        genderTextField.inputView = picker

    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        birthDateTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    var sender  = UIButton()

    @IBAction func signinButtonTapped(_ sender: UIButton) {
        guard validation() == true else { return }
        self.sender = sender
        sender.startWhiteAnimating()
        register()
    }
    @IBAction func genderTapped(_ sender: UIButton) {
        view.endEditing(true)
        UIView.animate(withDuration: 0.3) {
            self.picker.alpha = 1
        }
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Private Helpers
    
    fileprivate func validation() -> Bool {
        let title = "Validaton Error"
        
        if let mssg = emailTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = emailTextField.validator.isValidEmail(){
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = passwordTextField.validator.verifyPassword(With: confirmPasswordTextField) {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = passwordTextField.validator.isValidePassword() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = nameTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = addressTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = mobileTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = bloodTypeTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = birthDateTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        if let mssg = nationalIdTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        if let mssg = genderTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }


        return true
    }
    
    
    func register() {
        var gender = 1
        if let mssg = genderTextField.text , mssg == "Male" {
            gender = 0
        }

        let headers = [ "Accept": "application/json"]
        let parameterssss = [
            "email": "\(emailTextField.text!)",
            "password": "\(passwordTextField.text!)",
            "name": "\(nameTextField.text!)",
            "address": "\(addressTextField.text!)",
            "mobile":  "\(mobileTextField.text!)",
            "blood_type": "\(bloodTypeTextField.text!)",
            "birth_date" : "\(birthDateTextField.text!)",
            "national_id": "\(nationalIdTextField.text!)",
            "gender":"\(gender)"        ]

        
        let URL = try! URLRequest(url: basedomain+"auth/register", method: .post, headers: headers)

        Alamofire.upload(
            multipartFormData: { multipartFormData in
            
                for (key, value) in parameterssss {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
        },
            with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let request, _, _):
                    print("success")
                    request.uploadProgress(closure: { (progress_) in
                        print("Upload Progress: \(progress_.fractionCompleted)")
                        print("Upload totalUnitCount: \(progress_.totalUnitCount)")
                    })
                    request.validate(statusCode: 200..<500)
                    request.responseJSON { response in
                        
                        debugPrint("uploadRegistration: \(response)")
                        if response.result.isSuccess {
                            let dict = response.result.value! as! Dictionary<String, Any>
                            accessToken = dict["access_token"] as! String
                            let vc = self.storyboard!.instantiateViewController(withIdentifier: "loading")
                            self.present(vc, animated: true, completion: nil)
                        } else { //FAILURE
                            DispatchQueue.main.async {
                                self.sender.stopAnimating()
                                self.showErrorAlert(withTitle: "Error", message: "SomeThing went wrong");
                            }

                        }
                        
                    }
                case .failure(let errorType):
                    DispatchQueue.main.async {
                        self.sender.stopAnimating()
                        self.showErrorAlert(withTitle: " Error", message: "Connection Lost");
                        
                    }
                }
        })

    }
}

