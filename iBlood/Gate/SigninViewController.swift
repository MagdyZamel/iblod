//
//  SigninViewController.swift
//  iBlood
//
//  Created by Macbook Pro on 4/7/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit
import Alamofire

class SigninViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.text = "zz@aa.zz"
        passwordTextField.text = "Zamel@123"
//        emailTextField.text = "aya.amr9ee3e@gmail.com"
//        passwordTextField.text = "Test@123"

    }
    
    // MARK: - @IBActions
    var sender = UIButton()
    @IBAction func signinButtonTapped(_ sender: UIButton) {
    self.sender = sender
        if let messge =  self.emailTextField.validator.isValidEmail(){
            self.showErrorAlert(withMessage: messge)
            return
        }
        if let dsd =  self.passwordTextField.validator.isValidePassword(){
            self.showErrorAlert(withMessage: dsd)
            return
        }
        sender.startWhiteAnimating()
        register()
    }
    
    func register() {
        let headers = [ "Accept": "application/json"]
        let parameterssss = [
            "email": "\(emailTextField.text!)",
            "password": "\(passwordTextField.text!)"
        ]
        
        let URL = try! URLRequest(url: basedomain+"auth/login", method: .post, headers: headers)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for (key, value) in parameterssss {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
        },
            with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let request, _, _):
                    print("success")
                    request.uploadProgress(closure: { (progress_) in
                        print("Upload Progress: \(progress_.fractionCompleted)")
                        print("Upload totalUnitCount: \(progress_.totalUnitCount)")
                    })
                    request.validate(statusCode: 200..<500)
                    request.responseJSON { response in
                        
                        debugPrint("uploadRegistration: \(response)")
                        if response.result.isSuccess {
                            let dict = response.result.value! as! Dictionary<String, Any>
                            accessToken = dict["access_token"] as! String
                            self.presentHome()

                        } else { //FAILURE
                            DispatchQueue.main.async {
                                self.sender.stopAnimating()
                                self.showErrorAlert(withTitle: "Error", message: "SomeThing went wrong");
                            }
                            
                        }
                        
                    }
                case .failure(let errorType):
                    DispatchQueue.main.async {
                        self.sender.stopAnimating()
                        self.showErrorAlert(withTitle: " Error", message: "Connection Lost");
                        
                    }
                }
        })
        
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func presentHome(){
        let vc = storyboard!.instantiateViewController(withIdentifier: "loading")
        self.present(vc, animated: true, completion: nil)
    }
    
}


