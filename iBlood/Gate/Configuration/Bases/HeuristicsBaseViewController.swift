//
//  HeuristicsBaseViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/19/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

 protocol ViewControllerMiddleware: class {
     func setup()
     func setupViews()
}

class HeuristicsBaseViewController: UIViewController, ViewControllerMiddleware {

    func setupViews() {
    }
    
    func setup() {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setup()
    }
    
    
    func presentAlertWith( title:String, message:String, actionTitle:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    func presentAlertWith(title:String, message:String, actionTitle:String,action:((UIAlertAction) -> Void)? ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertActionStyle.default, handler: action))
        self.present(alert, animated: true, completion: nil)
        
    }

    
    
}
