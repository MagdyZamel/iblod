//
//  BaseTabBarViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/13/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class BaseTabBarViewController: UITabBarController {
    
    var selectionView:UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //let chatListVC = ChatSectionStoryboard.initialViewController()
        //viewControllers![3] = chatListVC
        self.delegate = self
        self.configUI()
        UIApplication.shared.statusBarStyle = .lightContent

    }
    
    func configUI() {
        for item in self.tabBar.items ?? [] {
            item.title = ""
            item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        }
        
        selectionView = UIView(frame: CGRect(x: 0, y: 0.5, width: self.tabBar.frame.size.width/5, height: 3))
        selectionView!.backgroundColor = UIColor.white
        self.tabBar.addSubview(selectionView!)
    }
    
    func changeTabBarIndex(index: Int) {
        let itemSize = self.tabBar.frame.size.width/5
        
        UIView.animate(withDuration: 0.2) { [weak self] in
            if index != 2 {
                self?.selectionView?.frame.origin.x = CGFloat(index) * itemSize
                self?.selectedIndex = index
            }
        }
    }
    
}

extension BaseTabBarViewController: UITabBarControllerDelegate {
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index = tabBar.items?.index(of: item) ?? 0
        let itemSize = self.tabBar.frame.size.width/5
        
        UIView.animate(withDuration: 0.2) { [weak self] in
            if index != 2 {
                self?.selectionView?.frame.origin.x = CGFloat(index) * itemSize
            }
        }
        
        if tabBar.items?.index(of: item) ?? 0  == 2 {
            let storyboard = UIStoryboard(name: "Camera", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CamVC")
//            self.presentVC(vc)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let nav = viewController as? UINavigationController {
            return nav.title != "Camera"
        }
        return true
    }
    
}
