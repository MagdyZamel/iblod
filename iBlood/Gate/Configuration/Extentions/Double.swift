//
//  Float.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 6/19/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import Foundation

extension Double {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
