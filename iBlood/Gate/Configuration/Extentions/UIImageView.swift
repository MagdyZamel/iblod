//
//  UIImageView.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/20/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
//import Kingfisher

extension UIImageView {
    func loadImage(_ url: String, withLoadingColor color: UIColor = UIColor.white,_ placeholder: UIImage = #imageLiteral(resourceName: "avatar1")) {
        if let url = URL(string: url){
//            self.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.2))])
            return
        }
        self.image = placeholder
    }
}
extension UIImage {
    var jpeg: Data? {
        return UIImageJPEGRepresentation(self, 0.5)
    }
    var png: Data? {
        return UIImagePNGRepresentation(self)
    }
}
