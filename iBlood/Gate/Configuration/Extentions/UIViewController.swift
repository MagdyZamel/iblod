//
//  UIViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/19/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

import CDAlertView

extension UIViewController {
    
    func showErrorAlert(withTitle title: String, message: String){
        let alert = CDAlertView(title: title, message: message, type: .error)
        let doneAction = CDAlertViewAction(title: "Cancel")
        alert.add(action: doneAction)
        alert.show()
    }
    func showSuccessAlert(withTitle title: String, message: String){
        let alert = CDAlertView(title: title, message: message, type: .success)
        let doneAction = CDAlertViewAction(title: "Cancel")
        alert.add(action: doneAction)
        alert.show()
    }

    func showErrorAlert(withMessage message: String){
        let title = "Unfortunately"
        showErrorAlert(withTitle: title, message: message)
    }
    
    @IBAction func reload(){
        self.viewDidLoad()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func backTapped(_ sender: Any?) {
        if let _  = self.navigationController?.popViewController(animated: true){
            
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
}

