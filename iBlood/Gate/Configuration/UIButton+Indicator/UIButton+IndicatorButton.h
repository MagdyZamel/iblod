//
//  UIButton+IndicatorButton.h
//  RIndicatorButton
//
//  Created by Ibrahim Al-Khayat on 4/27/14.
//  Copyright (c) 2014 Robusta studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (IndicatorButton)

@property CGFloat originalAlpha;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;

- (void)initIndicatorWithActivityIndicatorStyle:(UIActivityIndicatorViewStyle)style;
- (void)startAnimating;
- (void)startWhiteAnimating;
-(void)startAnimatingWithColor:(UIColor*) color;
- (void)stopAnimating;

@end
