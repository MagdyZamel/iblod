//
//  LoadingViewController.swift
//  iBlood
//
//  Created by Macbook Pro on 4/7/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {

    @IBOutlet weak var logoStackView: UIStackView!
    @IBOutlet weak var mainImgView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    
    var firstTimeFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidLayoutSubviews() {
        if firstTimeFlag == false {
            firstTimeFlag = true
            setupView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupAnimations()
    }
    
    func configData() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.presentHome()
        }
    }
    
    fileprivate func presentHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        self.present(vc!, animated: true, completion: nil)
        
    }
    
    func setupView() {
        let viewPosition = self.view.center.y - logoStackView.center.y
        logoStackView.transform = CGAffineTransform(translationX: 0, y: viewPosition)
        stackView.transform = CGAffineTransform(translationX: 0, y: 400)
    }
    
    func setupAnimations() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            UIView.animate(withDuration: 1, animations: { [weak self] in
                self?.logoStackView.transform = CGAffineTransform.identity
                self?.stackView.transform = CGAffineTransform.identity
            }, completion: {[weak self] (success) in
                self?.configData()
            })
        })
    }
}
