//
//  SingleRequest.swift
//  iBlood
//
//  Created by Mostafa El-Sayed on 5/15/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import Foundation
import ObjectMapper

struct SingleRequest : Mappable {
    var id : Int?
    var user_id : Int?
    var lat : Double?
    var long : Double?
    var address : String?
    var emergency_name : String?
    var emergency_mobile : String?
    var emergency_national_id : String?
    var emergency_blood_type : String?
    var feedback : String?
    var rating : String?
    var created_at : String?
    var updated_at : String?
    var is_accepted : Bool?
    var accepted_notifications : [String]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        user_id <- map["user_id"]
        lat <- map["lat"]
        long <- map["long"]
        address <- map["address"]
        emergency_name <- map["emergency_name"]
        emergency_mobile <- map["emergency_mobile"]
        emergency_national_id <- map["emergency_national_id"]
        emergency_blood_type <- map["emergency_blood_type"]
        feedback <- map["feedback"]
        rating <- map["rating"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        is_accepted <- map["is_accepted"]
        accepted_notifications <- map["accepted_notifications"]
    }
    
}
