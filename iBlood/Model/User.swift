//
//  User.swift
//  iBlood
//
//  Created by Ali Hamed on 5/15/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import Foundation
import ObjectMapper

class User: NSObject, NSCoding, Mappable{
    
    var address : String?
    var avatar : String?
    var birthDate : String?
    var bloodType : String?
    var createdAt : String?
    var email : String?
    var gender : Int?
    var id : Int?
    var mobile : String?
    var name : String?
    var nationalId : String?
    var updatedAt : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return User()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        address <- map["address"]
        avatar <- map["avatar"]
        birthDate <- map["birth_date"]
        bloodType <- map["blood_type"]
        createdAt <- map["created_at"]
        email <- map["email"]
        gender <- map["gender"]
        id <- map["id"]
        mobile <- map["mobile"]
        name <- map["name"]
        nationalId <- map["national_id"]
        updatedAt <- map["updated_at"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        avatar = aDecoder.decodeObject(forKey: "avatar") as? String
        birthDate = aDecoder.decodeObject(forKey: "birth_date") as? String
        bloodType = aDecoder.decodeObject(forKey: "blood_type") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? Int
        id = aDecoder.decodeObject(forKey: "id") as? Int
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        nationalId = aDecoder.decodeObject(forKey: "national_id") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if avatar != nil{
            aCoder.encode(avatar, forKey: "avatar")
        }
        if birthDate != nil{
            aCoder.encode(birthDate, forKey: "birth_date")
        }
        if bloodType != nil{
            aCoder.encode(bloodType, forKey: "blood_type")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if nationalId != nil{
            aCoder.encode(nationalId, forKey: "national_id")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        
    }
    
}
