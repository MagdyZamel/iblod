//
//  AcceptedUserVC.swift
//  iBlood
//
//  Created by Mostafa El-Sayed on 5/15/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit

class AcceptedUserVC: UIViewController {

    @IBOutlet weak var noAcceptedView: UIView!
    @IBOutlet weak var tableView: UITableView!
    private var cellReusableID = "AcceptedUserViewCell"
    var requestID = 0
    var users = [User]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        getAcceptedUserList()
    }
    func getAcceptedUserList() {
        let headers = [
            "Authorization": "Bearer \(accessToken)",
            "Accept": "application/json",
            "cache-control": "no-cache",
            "Postman-Token": "d171615d-f18a-4714-aef6-ae3c57522bbf"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(basedomain)requests/\(requestID)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                var json: NSDictionary = [:]
                guard let data = data else {
                    return
                }
                
                do {
                    var requests = [User]()
                    guard let ssss =  try! JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
                        self.noAcceptedView.isHidden = false
                        return
                    }
                    ssss.forEach({ (request) in
                        if let x = User(JSON: request) {
                            requests.append(x)
                        }
                    })
                
                    
                    DispatchQueue.main.async {
                        if requests.count == 0 {
                            self.noAcceptedView.isHidden = false
                            self.tableView.isHidden = true
                        } else {
                            self.noAcceptedView.isHidden = true
                            self.tableView.isHidden = false
                        }
                        self.users = requests
                        self.tableView.reloadData()
                    }
                } catch {
                    // Do nothing
                }
            }
        })
        
        dataTask.resume()
    }
    func setupViews(){
        self.navigationController?.navigationItem.largeTitleDisplayMode = .automatic
        setupSearchBar()
        setupNavigationButtons()
        configureTableView()
    }
    
    func setupSearchBar() {
        self.navigationItem.title = "List Accepted Users"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        let searchController = UISearchController(searchResultsController: nil)
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController?.dimsBackgroundDuringPresentation = false
    }
    
    func setupNavigationButtons() {
        let emergencyButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
        emergencyButton.setTitleColor(#colorLiteral(red: 1, green: 0.2812636793, blue: 0.3673964143, alpha: 1), for: [])
        emergencyButton.setTitle("Close", for: [])
        emergencyButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
        emergencyButton.addTarget(self, action: #selector(self.emergencyButtonTapped), for: UIControl.Event.touchUpInside)
        let leftBarButton = UIBarButtonItem(customView: emergencyButton)
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    @objc func emergencyButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addButtonTapped() {
        let newRequestVC = NewRequestViewController()
        self.navigationController?.pushViewController(newRequestVC, animated: true)
    }
    
    func configureTableView() {
        let postsCell = UINib(nibName: cellReusableID, bundle: nil)
        tableView.register(postsCell, forCellReuseIdentifier: cellReusableID)
    }
    
}

extension AcceptedUserVC: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReusableID, for: indexPath) as! AcceptedUserViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.white
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 13
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let feedback = UITableViewRowAction(style: .default, title: "Feedback") { action, index in
            print("hello")
        }
        feedback.backgroundColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        return [feedback]
    }
}

extension AcceptedUserVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


