//
//  AcceptedUserViewCell.swift
//  iBlood
//
//  Created by Aya Amr on 4/23/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit

class AcceptedUserViewCell: UITableViewCell {

    @IBOutlet weak var blood: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var useImage: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
