//
//  ProfileViewController.swift
//  iBlood
//
//  Created by Ali Hamed on 5/15/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class ProfileViewController: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var mobiletextField: UITextField!
    @IBOutlet weak var bloodTypeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var nationalIDTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var sender: UIActivityIndicatorView!
    @IBOutlet weak var scroll: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserInfo()
    }
    
    func setUserInfo(user: User) {
        nameTextField.text = user.name
        addressTextField.text = user.address
        mobiletextField.text = user.mobile
        bloodTypeTextField.text = user.bloodType
        emailTextField.text = user.email
        ageTextField.text = user.birthDate
        nationalIDTextField.text = user.nationalId
        genderTextField.text = (user.gender ?? 0) == 0 ? "Male" : "Female"
    }
    
    func getUserInfo() {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let manager = Manager()
            let urlString = "\(hostName)favourites"
            let headers = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            let request = NSMutableURLRequest(url: NSURL(string: "\(basedomain)profile")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    DispatchQueue.main.async {
                        self.sender.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.showErrorAlert(withTitle: " Error", message: "Connection Lost");
                    }
                    
                    
                } else {
                    DispatchQueue.main.async {
                        let httpResponse = response as? HTTPURLResponse
                        self.sender.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false

                        if httpResponse?.statusCode == 200 {
                            self.scroll.isHidden = false
                            let ssss =  try! JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
                            self.setUserInfo(user: User(JSON: ssss)!)

                        } else {
                            self.showErrorAlert(withTitle: " Error", message: "Something went wrong");

                        }
                    }
                }
            })
            
            dataTask.resume()
        

    }
    
}
