//
//  NewsFeedViewCell.swift
//  iBlood
//
//  Created by Mostafa El-Sayed on 5/15/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit

class NewsFeedViewCell: UITableViewCell {

    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
