//
//  NewsFeedVC.swift
//  iBlood
//
//  Created by Mostafa El-Sayed on 5/15/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit

class NewsFeedVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var cellReusableID = "NewsFeedViewCell"
    private var searchController:UISearchController!
    var images = ["news1","news2","news3","news4","news5"]
    var titles = [
        "IS Blood Donation Safe??",
        "What are the benefits of blood donation??",
        "How long does it take to recover from donating blood?",
        "Why blood donation?",
        "what are the process of blood donation"
    ]
    var desc = [
        "Blood donation is safe. New, sterile disposable equipment is used for each donor, so there's no risk of contracting a bloodborne infection by donating blood. ... Within 24 hours of a blood donation, your body replaces the lost fluids. And after several weeks, your body replaces the lost red blood cells.",
         "1-Blood regeneration\n2-Reduce the risk of many cancers\n3-Reduce the risk of heart clots\n4-Diseases and treatment of blood donation\n5-Maintain liver integrity\n6-Reduce oxidative factors and inflammatory stimuli\n7-Psychological benefits are important\n8-FREE BIOLOGICAL ANALYSIS\n9-A slight contribution to weight loss\n10-The benefits of launching at very little price",
         " How long will it take to replenish the pint of blood I donate? The plasma from your donation is replaced within about 24 hours. Red cells need about four to six weeks for complete replacement. That's why at least eight weeks are required between whole blood donations.",
         "Blood donation is a noble humanitarian act because it contributes to saving the lives of thousands of patients who are in desperate need of blood transfusion. The fact is that one out of 10 patients enters the hospital in need of transfusion, especially patients suffering from malignant or intractable diseases, as well as those who are exposed to serious accidents that have lost a large amount of blood, as well as many patients during major surgeries, in addition to that the components of blood used in the treatment of many serious diseases.",
        "Once the pre-donation screening is finished, you will proceed to a donor bed where your arm will be cleaned with an antiseptic, and a professional will use a blood donation kit to draw blood from a vein in your arm. ... During the donation process, you will donate one unit of blood; this takes about six to ten minutes."]
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews(){
        self.navigationController?.navigationItem.largeTitleDisplayMode = .automatic
        setupSearchBar()
        setupNavigationButtons()
        configureTableView()
    }
    
    func setupSearchBar() {
        self.navigationItem.title = "News Feed"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController?.dimsBackgroundDuringPresentation = false
    }
    
    func setupNavigationButtons() {
//        let emergencyButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
//        emergencyButton.setTitleColor(#colorLiteral(red: 1, green: 0.2812636793, blue: 0.3673964143, alpha: 1), for: [])
//        emergencyButton.setTitle("Emergency", for: [])
//        emergencyButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
//        emergencyButton.addTarget(self, action: #selector(self.emergencyButtonTapped), for: UIControl.Event.touchUpInside)
//        let leftBarButton = UIBarButtonItem(customView: emergencyButton)
//        self.navigationItem.leftBarButtonItem = leftBarButton
//        
//        let addRequestButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
//        addRequestButton.setImage(#imageLiteral(resourceName: "plus"), for: [])
//        addRequestButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
//        addRequestButton.addTarget(self, action: #selector(self.addButtonTapped), for: UIControl.Event.touchUpInside)
//        let rightBarButton = UIBarButtonItem(customView: addRequestButton)
//        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func emergencyButtonTapped() {
        let emergencyVC = EmergencyViewController()
        self.navigationController?.pushViewController(emergencyVC, animated: true)
    }
    
    @objc func addButtonTapped() {
        let newRequestVC = NewRequestViewController()
        self.navigationController?.pushViewController(newRequestVC, animated: true)
    }
    
    func configureTableView() {
        let postsCell = UINib(nibName: cellReusableID, bundle: nil)
        tableView.register(postsCell, forCellReuseIdentifier: cellReusableID)
    }
    
}

extension NewsFeedVC: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return images.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReusableID, for: indexPath) as! NewsFeedViewCell
        cell.feedImage.image = UIImage(named: images[indexPath.section])
        cell.title.text = titles[indexPath.section]
        cell.desc.text = desc[indexPath.section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.white
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let feedback = UITableViewRowAction(style: .default, title: "Feedback") { action, index in
            print("hello")
        }
        feedback.backgroundColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        return [feedback]
    }
}

extension NewsFeedVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let acceptedUserVC = AcceptedUserVC()
//        let nav = UINavigationController(rootViewController: acceptedUserVC)
//        self.present(nav, animated: true, completion: nil)
    }
}

extension NewsFeedVC: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            //            presenter.updateSearchResults(text: searchText)
        }
    }
}

