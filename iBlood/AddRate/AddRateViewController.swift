//
//  AddRateViewController.swift
//  iBlood
//
//  Created by Ali Hamed on 5/16/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit
import Cosmos

protocol AddRateViewControllerDelegate: class {
    func didAddRate(rate: Double, comment: String)
}

class AddRateViewController: UIViewController {
    
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var textView: UITextView!
    weak var delegate: AddRateViewControllerDelegate?
    var rate: Double = 3.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        displayExtraNotesPlaceHolder()
    }
    
    func didFinishRating(rate: Double){
        self.rate = rate
    }
    
    @IBAction func addButtonClicked(_ sender: UIButton) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true || textView.textColor == UIColor.darkGray {
            self.showErrorAlert(withTitle: "warning", message: "please insert comment")
        }else{
            self.addButton.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5 ) {
                self.showSuccessAlert(withTitle: "Success", message: "Your rate addedd successfully")
                self.addButton.stopAnimating()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func cancelButtonClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissButtonClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddRateViewController: UITextViewDelegate{
    
    func displayExtraNotesPlaceHolder() {
        textView.toolbarPlaceholder = "Write your comment here ..."
        textView.text = "Write your comment here ..."
        textView.textColor = UIColor.darkGray
    }
    
    func getAddedNotesText() -> String {
        if textView.textColor == UIColor.darkGray || textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            textView.text = " "
            return textView.text
        } else {
            return textView.text!
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.darkGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            textView.text = "Add Your Comment ..."
            textView.textColor = UIColor.darkGray
        }
    }
    
    func setTextFieldValue(){
        self.textView.text = "Write your comment here ..."
        self.textView.textAlignment = .left
    }

}
