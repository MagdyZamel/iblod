//
//  Manager.swift
//  iBlood
//
//  Created by Ali Hamed on 5/15/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

let hostName = "http://138.68.102.152/app/public/index.php"

enum ServiceName: String {
    case userProfile = "/api/profile"
}

class Manager {
    
    func perform(methodType: HTTPMethod = .post, useCustomeURL: Bool = false, urlStr: String = "", serviceName: ServiceName, parameters: [String: AnyObject]? = nil, completionHandler: @escaping (Any?, String?) -> Void)-> Void {
        
        var urlString: String = ""
        var headers: HTTPHeaders? = nil
        
        if useCustomeURL {
            urlString = urlStr
        }else {
            urlString = "\(hostName)\(serviceName.rawValue)"
        }
        
        print("ServiceName:\(serviceName)  parameters: \(String(describing: parameters))")
        
        headers = [
            "X-localization": Locale.preferredLanguages[0],
            "Authorization": "Bearer "
        ]
        
//        if User.shared.token != "" && User.shared.token != nil{
//            headers = [
//                "X-localization": Locale.preferredLanguages[0],
//                "Authorization": "Bearer \(User.shared.token!)"
//            ]
//        } else {
//            headers = [
//                "X-localization": Locale.preferredLanguages[0]
//            ]
//        }
        
        
        Alamofire.request(urlString, method: methodType, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in

            debugPrint(response)
            
            if response.result.isSuccess {
                
                let dict = response.result.value! as! [String: Any]
                
                if dict["value"] as? Bool == true || dict["value"] as? String == "true" {
                    completionHandler(dict, nil)
                }else{
                    if let dictError = dict["error"] as? String {
                        completionHandler(nil, dictError)
                    }else {
                        guard let errorStr = dict["msg"] as? String else {
                            let errorsDict = dict["msg"] as! [String: Any]
                            let errorsArr = errorsDict.values.first as! [String]
                            
                            completionHandler(nil, errorsArr[0])
                            return
                        }
                        completionHandler(nil, errorStr)
                    }
                }
                
            } else { //FAILURE
                print("error \(String(describing: response.result.error)) in serviceName: \(serviceName)")
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
    }
    
    func uploadImage(serviceName: ServiceName, profileImage: UIImage? = nil, parameters: [String: AnyObject]? = nil, progress: @escaping (_ percent: Float) -> Void, completionHandler: @escaping (Any?, String?) -> Void) {
        
        var headers: HTTPHeaders? = nil
        
//        if User.shared.token != "" && User.shared.token != nil{
//            headers = [
//                "X-localization": Locale.preferredLanguages[0],
//                "Authorization": "Bearer \(User.shared.token!)"
//            ]
//        }else{
//            headers = [
//                "X-localization": Locale.preferredLanguages[0]
//            ]
//        }
        
        let urlString: String =  "\(hostName)\(serviceName.rawValue)"
        
        
        
        let URL = try! URLRequest(url: urlString, method: .post, headers: headers)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                if(profileImage != nil ){
//                    if  let profileImageData = profileImage?.da {
//                        multipartFormData.append(profileImageData , withName: "image", fileName: "image0.jpg", mimeType: "image/jpg")
//                    }
                }
                
                for (key, value) in parameters! {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
        },
            with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let request, _, _):
                    print("success")
                    request.uploadProgress(closure: { (progress_) in
                        print("Upload Progress: \(progress_.fractionCompleted)")
                        print("Upload totalUnitCount: \(progress_.totalUnitCount)")
                    })
                    request.validate(statusCode: 200..<500)
                    request.responseJSON { response in
                        
                        debugPrint("uploadRegistration: \(response)")
                        if response.result.isSuccess {
                            request.uploadProgress(closure: { (progress_) in
                                print("Upload Progress: \(progress_.fractionCompleted)")
                                print("Upload totalUnitCount: \(progress_.totalUnitCount)")
                            })
                            let dict = response.result.value! as! Dictionary<String, Any>
                            
                            print(response.result)
                            
                            if (dict["value"] as! Bool == false){
                                let errorMsg = dict["msg"] as! String
                                completionHandler(nil,errorMsg)
                                
                            }else{
                                let statusCode = response.response?.statusCode
                                if statusCode! >= 200 && statusCode! <= 300 {
                                    completionHandler(dict, nil)
                                }else{
                                    completionHandler(nil, "Something Went Wrong.")
                                }
                                
                            }
                        }else { //FAILURE
                            print("error \(String(describing: response.result.error)) in serviceName: Upload Image")
                            completionHandler(nil, response.result.error?.localizedDescription)
                        }
                        
                    }
                case .failure(let errorType):
                    print("encodingError:\(errorType)")
                }
        })
        
    }

}
