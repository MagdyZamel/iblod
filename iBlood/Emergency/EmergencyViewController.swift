//
//  EmergencyViewController.swift
//  iBlood
//
//  Created by Aya Amr on 4/23/19.
//  Copyright © 2019 mohamedAmr. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
class EmergencyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
    }
    
    func setupSearchBar() {
        self.navigationItem.title = "Emergency"
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }

    @IBAction func shareButtonTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Location", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    var location: CLLocationCoordinate2D?
    @IBOutlet weak var locationTextField: UITextField!
    
    
    
    @IBOutlet weak var senderqqqqqq : UIButton!
    @IBOutlet weak var emergency_name : UITextField!
    @IBOutlet weak var emergency_national_id : UITextField!
    @IBOutlet weak var emergency_blood_type : UITextField!
    @IBOutlet weak var emergency_mobile : UITextField!

    @IBAction func sendButtonTapped(_ sender: UIButton) {
        if self.locationTextField.text!.count < 1{
            self.showErrorAlert(withMessage: "Enter Request Location")
            return
        }
        if let mess =  emergency_name.validator.isNonEmpty() {
            self.showErrorAlert(withMessage: mess)
            return
        }
        if let mess =  emergency_national_id.validator.isNonEmpty() {
            self.showErrorAlert(withMessage: mess)
            return
        }
        if let mess =  emergency_blood_type.validator.isNonEmpty() {
            self.showErrorAlert(withMessage: mess)
            return
        }
        if let mess =  emergency_mobile.validator.isNonEmpty() {
            self.showErrorAlert(withMessage: mess)
            return
        }
        DispatchQueue.main.async {
            self.senderqqqqqq.startWhiteAnimating()
        }
        
        
        add()
    }
    
    func add() {
        let headers = [ "Accept": "application/json",
                        "Authorization": "Bearer \(accessToken)"
            
        ]
        
        let parameterssss = [
            "lat": "\(location!.latitude)",
            "long": "\(location!.latitude)",
            "address": locationTextField.text!,
            "emergency_name":emergency_name.text!,
            "emergency_national_id":emergency_national_id.text!,
            "emergency_blood_type":emergency_blood_type.text!,
            "emergency_mobile":emergency_mobile.text!
            
        ]
        
        let URL = try! URLRequest(url: basedomain+"requests", method: .post, headers: headers)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for (key, value) in parameterssss {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
        },
            with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let request, _, _):
                    print("success")
                    request.uploadProgress(closure: { (progress_) in
                        print("Upload Progress: \(progress_.fractionCompleted)")
                        print("Upload totalUnitCount: \(progress_.totalUnitCount)")
                    })
                    request.validate(statusCode: 200..<500)
                    request.responseJSON { response in
                        
                        debugPrint("uploadRegistration: \(response)")
                        if response.result.isSuccess {
                            DispatchQueue.main.async {
                                self.senderqqqqqq.stopAnimating()
                                self.showSuccessAlert(withTitle: "Success", message: "Your request has been created");
                            }
                            
                        } else { //FAILURE
                            DispatchQueue.main.async {
                                self.senderqqqqqq.stopAnimating()
                                self.showErrorAlert(withTitle: "Error", message: "SomeThing went wrong");
                            }
                            
                        }
                        
                    }
                case .failure(let errorType):
                    DispatchQueue.main.async {
                        self.senderqqqqqq.stopAnimating()
                        self.showErrorAlert(withTitle: " Error", message: "Connection Lost");
                        
                    }
                }
        })
        
    }
    
}

extension EmergencyViewController: LocationPikerDelegate {
    func LocationPiker(didPicked location: CLLocationCoordinate2D, locationDescription: String, placemark: CLPlacemark?) {
        self.location = location
        self.locationTextField.text = locationDescription
    }
    
    
}
